package com.pfe.promo.dao;

import com.pfe.promo.models.Promotion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPromotion extends JpaRepository<Promotion,Long> {



}
