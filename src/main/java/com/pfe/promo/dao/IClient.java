package com.pfe.promo.dao;

import com.pfe.promo.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClient extends JpaRepository<Client,Long>  {
}
