package com.pfe.promo.dao;

import com.pfe.promo.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IArticle extends JpaRepository<Article,Long> {

}