package com.pfe.promo.dao;

import com.pfe.promo.models.Client;
import com.pfe.promo.models.Commande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ICommande extends JpaRepository<Commande,Long> {
    List<Commande> findByClient(Client client);
}
