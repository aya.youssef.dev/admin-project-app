package com.pfe.promo.dao;

import com.pfe.promo.models.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
    public interface ICategorie extends JpaRepository<Categorie,Long> {

    }


