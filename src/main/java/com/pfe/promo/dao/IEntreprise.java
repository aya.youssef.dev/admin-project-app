package com.pfe.promo.dao;

import com.pfe.promo.models.Entreprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEntreprise extends JpaRepository<Entreprise,Long> {
}
