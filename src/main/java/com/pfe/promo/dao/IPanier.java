package com.pfe.promo.dao;

import com.pfe.promo.models.Panier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPanier extends JpaRepository<Panier,Long> {

}
