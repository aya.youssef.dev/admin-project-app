package com.pfe.promo.dao;

import com.pfe.promo.models.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Isubcategory extends JpaRepository<Subcategory,Long> {
}
