package com.pfe.promo.dao;

import com.pfe.promo.models.Livraison;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILivraison extends JpaRepository<Livraison,Long> {
}
