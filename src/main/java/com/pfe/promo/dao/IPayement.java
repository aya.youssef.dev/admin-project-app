package com.pfe.promo.dao;

import com.pfe.promo.models.Payement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPayement extends JpaRepository<Payement,Long> {
}
