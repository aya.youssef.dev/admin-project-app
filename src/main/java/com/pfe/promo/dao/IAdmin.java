package com.pfe.promo.dao;

import com.pfe.promo.models.Admin;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IAdmin extends JpaRepository<Admin,Long> {
}
