package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IArticle;

import com.pfe.promo.dao.IClient;
import com.pfe.promo.dao.IEntreprise;
import com.pfe.promo.dao.Isubcategory;
import com.pfe.promo.models.Article;

import com.pfe.promo.models.Client;
import com.pfe.promo.models.Entreprise;
import com.pfe.promo.models.Subcategory;
import com.pfe.promo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/article")
public class ArticleController {
    @Autowired
    private IArticle iarticle;

    @Autowired
    private IClient iclient;

    @Autowired
    private Isubcategory isubcategory;

    @Autowired
    private IEntreprise iEntreprise;


    @Autowired
    private StorageService storage;



    @GetMapping("/all")
    public List<Article> getAllarticles() {
        return iarticle.findAll();
    }

    @GetMapping("/getone/{id}")
    public Article getonearticles( @PathVariable Long id) {
        return iarticle.findOne(id);
    }


    @PostMapping("/save/{id_subcategory}/{id_entreprise}")
    public Article savearticleentreprise( Article ar,@PathVariable Long id_subcategory,@PathVariable Long id_entreprise,@RequestParam("file") MultipartFile file) {

        Subcategory su=isubcategory.findOne(id_subcategory);
        Entreprise e=iEntreprise.findOne(id_entreprise);

        ar.setSubcategory(su);
        ar.setEntreprise(e);

        storage.store(file);
        ar.setImage(file.getOriginalFilename());
        return iarticle.save(ar);
    }
    @PostMapping("/save2/{id_client}/{id_subcategory}")
    public Article savearticleclient( Article ar,@PathVariable Long id_client,@PathVariable Long id_subcategory,@RequestParam("file") MultipartFile file) {
        Client c=iclient .findOne(id_client);
        ar.setClient(c);
        Subcategory su=isubcategory.findOne(id_subcategory);
        ar.setSubcategory(su);


        storage.store(file);
        ar.setImage(file.getOriginalFilename());
        return iarticle.save(ar);
    }

    @PutMapping("/update/{Id}/{id_client}/{id_subcategory}")
    public Article update(@RequestBody Article ar,@PathVariable Long id_client,@PathVariable Long id_subcategory, @PathVariable Long Id) {
        Client c=iclient .findOne(id_client);
        ar.setClient(c);
        Subcategory su=isubcategory.findOne(id_subcategory);
        ar.setSubcategory(su);
        ar.setId(Id);
        return iarticle.saveAndFlush(ar);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteArticle(@PathVariable(value = "Id") Long Id) {
        HashMap message= new HashMap();
        try{
            iarticle.delete(Id);
            message.put("etat","article deleted");
            return message;
        }
        catch (Exception e) {
            message.put("etat","article not deleted");
            return message;
        }
    }


}
