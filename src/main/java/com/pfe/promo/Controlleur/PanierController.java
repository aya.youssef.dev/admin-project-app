package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IPanier;
import com.pfe.promo.models.Panier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/panier")
public class PanierController {
    @Autowired
    private IPanier ipanier;

    @GetMapping("/all")
    public List<Panier> getAllpaniers() {
        return ipanier.findAll();
    }

    @PostMapping("/save")
    public Panier savepanier(@RequestBody Panier pa) {
        return ipanier.save(pa);
    }

    @PutMapping("/update/{Id}")
    public Panier update(@RequestBody Panier pa, @PathVariable Long Id) {
        pa.setId(Id);
        return ipanier.saveAndFlush(pa);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deletePanier(@PathVariable(value = "Id") Long Id) {
        HashMap message = new HashMap();
        try {
            ipanier.delete(Id);
            message.put("etat", "panier deleted");
            return message;
        } catch (Exception e) {
            message.put("etat", "panier not deleted");
            return message;
        }
    }


}
