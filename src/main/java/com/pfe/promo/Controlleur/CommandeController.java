package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IClient;
import com.pfe.promo.dao.ICommande;
import com.pfe.promo.dao.IPanier;
import com.pfe.promo.dao.IPayement;
import com.pfe.promo.models.Client;
import com.pfe.promo.models.Commande;
import com.pfe.promo.models.Panier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/commande")
public class CommandeController {
    @Autowired
    private ICommande icommande;

    @Autowired
    private IPanier ipanier;

    @Autowired
    private IPayement iPayement;

    @Autowired
    private IClient iclient;

    @GetMapping("/all")
    public List<Commande> getAllcommandes() {
        return icommande.findAll();
    }

    @GetMapping("/allUserCommandes/{id_client}")
    public List<Commande> getAllUserCommandes(@PathVariable Long id_client) {
        Client c = iclient.findOne(id_client);
        return icommande.findByClient(c);
    }


    @PostMapping("/save/{id_panier}/{id_client}")
    public Commande savecommande(@RequestBody Commande co,@PathVariable Long id_panier,@PathVariable Long id_client) {
        iPayement.save(co.getPayement());
        Panier p =ipanier.findOne(id_panier);
        Client c=iclient.findOne(id_client);
        co.setClient(c);
        co.setPanier(p);
        return icommande.save(co);
    }

    @PutMapping("/update/{Id}")
    public Commande update(@RequestBody Commande co, @PathVariable Long Id) {
        co.setId(Id);
        return icommande.saveAndFlush(co);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deleteCommande(@PathVariable(value = "Id") Long Id) {
        HashMap message = new HashMap();
        try {
            icommande.delete(Id);
            message.put("etat", "commande deleted");
            return message;
        } catch (Exception e) {
            message.put("etat", "commande not deleted");
            return message;
        }
    }

}
