package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IAuthority;
import com.pfe.promo.dao.IEntreprise;
import com.pfe.promo.models.*;
import com.pfe.promo.utils.StorageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.logging.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/entreprise")
public class EntrepriseController {

    @Autowired
    private StorageService storage;

    @Autowired
    private IAuthority iAuthority;

    @Autowired
    private IEntreprise ientreprise;

    Logger logger = java.util.logging.Logger.getLogger(EntrepriseController.class.getName());


    @GetMapping("/all")
    public List<Entreprise> getAllentreprises() {
        return ientreprise.findAll();
    }

    @GetMapping("/getone/{id}")
    public Entreprise getoneentreprise(@PathVariable Long id) {
        return ientreprise.findOne(id);
    }



    @PostMapping("/save")
    public ResponseEntity<Entreprise> saveentreprise(@RequestBody Entreprise entreprise) {
        Entreprise savedEntreprise = null;
        logger.info("start checking post");
        try {
            entreprise = ientreprise.save(entreprise);
        } catch (Exception exception) {
            logger.log(Level.parse("une contrainte n'a pas été respecté"),exception.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (entreprise != null) {
            return new ResponseEntity(savedEntreprise, HttpStatus.OK);
        } else return new ResponseEntity("l entreprise n pas etait sauvgardé", HttpStatus.BAD_REQUEST);
    }


    @PutMapping("/update/{Id}")
    public Entreprise update(@RequestBody Entreprise e, @PathVariable Long Id) {
        e.setId(Id);
        return ientreprise.saveAndFlush(e);
    }

        @RequestMapping("/registerentreprise")
    public ResponseEntity<?> register(Entreprise user, @RequestParam("file") MultipartFile file )
    {
        Authority authority=new Authority();
        authority.setName("ENTREPRISE");
        iAuthority.save(authority);
        user.setRole("ENTREPRISE");
        user.setEnabled(true);
        user.setAuthorities(authority);
        user.setPassword(hash(user.getPassword()));
        storage.store(file);
        user.setImage(file.getOriginalFilename());

        ientreprise.save(user);

        return ResponseEntity.ok(new UserTokenState(null, 0, user));

    }
    String hash(String password) {


        String hashedPassword = null;
        int i = 0;
        while (i < 5) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            hashedPassword = passwordEncoder.encode(password);
            i++;
        }

        return hashedPassword;
    }




    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteEntreprise(@PathVariable(value = "Id") Long Id) {
        HashMap message= new HashMap();
        try{
            ientreprise.delete(Id);
            message.put("etat","entreprise deleted");
            return message;
        }
        catch (Exception e) {
            message.put("etat","entreprise not deleted");
            return message;
        }
    }
}
