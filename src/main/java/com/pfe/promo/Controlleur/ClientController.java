package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IAuthority;
import com.pfe.promo.dao.IClient;
import com.pfe.promo.exceptionPromo.BadEntryException;
import com.pfe.promo.models.Authority;
import com.pfe.promo.models.Client;
import com.pfe.promo.models.UserTokenState;
import com.pfe.promo.utils.StorageService;
import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/client")
public class ClientController {
    Logger logger = LoggerFactory.getLogger(ClientController.class);
    @Autowired
    private IClient iclient;


    @Autowired
    private IAuthority iAuthority;


    @Autowired
    private StorageService storage;

    @GetMapping("/all")
    public List<Client> getAllclients() {
        return iclient.findAll();
    }

    @GetMapping("/getone/{id}")
    public ResponseEntity<Client> getoneclients(@PathVariable Long id) throws BadEntryException {
        logger.trace("start checking id");
        Client client = null;
        ResponseEntity response=null;
 if (id == null || id.equals(0L)) {
     logger.error("id must be not empty nor null");
     response = new ResponseEntity("bad rrquest", HttpStatus.BAD_REQUEST);
 }
       else {
        try {
                client = iclient.findOne(id);
                if (client != null) {
                    response = new ResponseEntity(client ,HttpStatus.OK);
                }
            } catch (Exception e) {
                throw new BadEntryException("bad entry" + e.getStackTrace());
            }
        }
        return  response;
    }

    @PostMapping("/save")
    public Client saveclient(@RequestBody Client c) {
        return iclient.save(c);
    }

    @PutMapping("/update/{Id}")
    public Client update(@RequestBody Client c, @PathVariable Long Id) {
        c.setId(Id);
        Client c1=iclient.findOne(Id);
        c.setEnabled(true);
        c.setPassword(c1.getPassword());
        c.setUsername(c1.getUsername());
        c.setImage(c1.getImage());
        c.setRole((c1.getRole()));
        Authority authority=new Authority();
        authority.setName("CLIENT");
        iAuthority.save(authority);
        c.setAuthorities(authority);
        return iclient.saveAndFlush(c);
    }





    @RequestMapping("/registerclient")
    public ResponseEntity<?> register(Client user, @RequestParam("file") Optional<MultipartFile> file )
    {
        Authority authority=new Authority();
        authority.setName("CLIENT");
        iAuthority.save(authority);
        user.setRole("CLIENT");
        user.setEnabled(true);
        user.setAuthorities(authority);
        user.setPassword(hash(user.getPassword()));
        if (file.isPresent()){
        storage.store(file.get());
        user.setImage(file.get().getOriginalFilename());
        }
        iclient.save(user);

        return ResponseEntity.ok(new UserTokenState(null, 0, user));

    }



    String hash(String password) {

        String hashedPassword = null;
        int i = 0;
        while (i < 5) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            hashedPassword = passwordEncoder.encode(password);
            i++;
        }

        return hashedPassword;
    }



    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteClient(@PathVariable(value = "Id") Long Id) {
        HashMap message= new HashMap();
        try{
            iclient.delete(Id);
            message.put("etat","client deleted");
            return message;
        }
        catch (Exception e) {
            message.put("etat","client not deleted");
            return message;
        }
    }
    @PutMapping("/modifimage/{id}")
    public Client modif(@RequestParam("file") MultipartFile file, @PathVariable Long id){
        Client c = iclient.findOne(id);
        storage.store(file);
        c.setImage(file.getOriginalFilename());
        return iclient.saveAndFlush(c) ;
    }

}
