package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.ILivraison;
import com.pfe.promo.models.Livraison;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/livraison")
public class LivraisonController {
    @Autowired
    private ILivraison ilivraison;

    @GetMapping("/all")
    public List<Livraison> getAlllivraisons() {
        return ilivraison.findAll();
    }

    @PostMapping("/save")
    public Livraison savelivraison(@RequestBody Livraison l) {
        return ilivraison.save(l);
    }

    @PutMapping("/update/{Id}")
    public Livraison update(@RequestBody Livraison l, @PathVariable Long Id) {
        l.setId(Id);
        return ilivraison.saveAndFlush(l);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deleteLivraison(@PathVariable(value = "Id") Long Id) {
        HashMap message = new HashMap();
        try {
            ilivraison.delete(Id);
            message.put("etat", "livraison deleted");
            return message;
        } catch (Exception e) {
            message.put("etat", "livraison not deleted");
            return message;
        }
    }
}
