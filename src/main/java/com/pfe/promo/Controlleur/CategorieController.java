package com.pfe.promo.Controlleur;


import com.pfe.promo.dao.ICategorie;

import com.pfe.promo.models.Categorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/categorie")
    public class CategorieController {
        @Autowired
        private ICategorie icategorie;

        @GetMapping("/all")
        public List<Categorie> getAllcategories() {
            return icategorie.findAll();
        }

        @PostMapping("/save")
        public Categorie savecategories(@RequestBody Categorie c) {
            return icategorie.save(c);
        }

        @PutMapping("/update/{Id}")
        public Categorie update(@RequestBody Categorie c, @PathVariable Long Id) {
            c.setId(Id);
            return icategorie.saveAndFlush(c);
        }

        @DeleteMapping("/delete/{Id}")
        public HashMap<String,String> deleteCategorie(@PathVariable(value = "Id") Long Id) {
            HashMap message= new HashMap();
            try{
                icategorie.delete(Id);
                message.put("etat","Categorie deleted");
                return message;
            }
            catch (Exception e) {
                message.put("etat","Categorie not deleted");
                return message;
            }
        }


    }
