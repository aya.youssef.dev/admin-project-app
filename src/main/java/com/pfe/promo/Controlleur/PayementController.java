package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IPayement;
import com.pfe.promo.models.Payement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/payement")
public class PayementController {
    @Autowired
    private IPayement ipayement;

    @GetMapping("/all")
    public List<Payement> getAllpayements() {
        return ipayement.findAll();
    }

    @PostMapping("/save")
    public Payement savepayement(@RequestBody Payement pa) {
        return ipayement.save(pa);
    }

    @PutMapping("/update/{Id}")
    public Payement update(@RequestBody Payement pa, @PathVariable Long Id) {
        pa.setId(Id);
        return ipayement.saveAndFlush(pa);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deletePayement(@PathVariable(value = "Id") Long Id) {
        HashMap message = new HashMap();
        try {
            ipayement.delete(Id);
            message.put("etat", "payement deleted");
            return message;
        } catch (Exception e) {
            message.put("etat", "payement not deleted");
            return message;
        }
    }
}
