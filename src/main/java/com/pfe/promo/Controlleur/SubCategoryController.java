package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.ICategorie;
import com.pfe.promo.dao.Isubcategory;
import com.pfe.promo.models.Categorie;
import com.pfe.promo.models.Subcategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/subcategory")
public class SubCategoryController {

    @Autowired
    private Isubcategory isubcategory;

    @Autowired
    private ICategorie icategorie;

    @GetMapping("/all")
    public List<Subcategory> getAllsubcategorys() {
        return isubcategory.findAll();
    }

    @PostMapping("/save/{idcategory}")
    public Subcategory savecsubcategorys(@RequestBody Subcategory su,@PathVariable Long idcategory) {
        Categorie c =icategorie.findOne(idcategory);
        su.setCategorie(c);
        return isubcategory.save(su);
    }

    @PutMapping("/update/{Id}")
    public Subcategory update(@RequestBody Subcategory su, @PathVariable Long Id) {
        su.setId(Id);
        return isubcategory.saveAndFlush(su);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteCSubcategory(@PathVariable(value = "Id") Long Id) {
        HashMap message= new HashMap();
        try{
            isubcategory.delete(Id);
            message.put("etat","Subcategory deleted");
            return message;
        }
        catch (Exception e) {
            message.put("etat","Subcategory not deleted");
            return message;
        }
    }


}
