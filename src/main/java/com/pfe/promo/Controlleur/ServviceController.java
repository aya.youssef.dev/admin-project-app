package com.pfe.promo.Controlleur;


import com.pfe.promo.dao.IEntreprise;
import com.pfe.promo.dao.IServvice;
import com.pfe.promo.dao.Isubcategory;
import com.pfe.promo.models.Entreprise;
import com.pfe.promo.models.Servvice;
import com.pfe.promo.models.Subcategory;
import com.pfe.promo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/servvice")


public class ServviceController {
    @Autowired
    private StorageService storage;
    @Autowired
    private IServvice iservvice;

    @Autowired
    private  Isubcategory isubcategory;

    @Autowired
    private IEntreprise ientreprise;

    @GetMapping("/all")
    public List<Servvice> getAllservvices() {
        return iservvice.findAll();
    }

    @GetMapping("/getone/{id}")
    public Servvice getoneservvice( @PathVariable Long id) {
        return iservvice.findOne(id);
    }

    @PostMapping("/save/{id_subcategory}/{id_entreprise}")

    public Servvice savecservvices( Servvice se,@PathVariable Long id_subcategory,@PathVariable Long id_entreprise,@RequestParam("file") MultipartFile file) {

        Subcategory su=isubcategory.findOne(id_subcategory);
        Entreprise e=ientreprise.findOne(id_entreprise);

        se.setSubcategory(su);
        se.setEntreprise(e);

        storage.store(file);
        se.setImage(file.getOriginalFilename());
        return iservvice.save(se);
    }


    @PutMapping("/update/{Id}")
    public Servvice update(@RequestBody Servvice se, @PathVariable Long Id) {
        se.setId(Id);
        return iservvice.saveAndFlush(se);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteServvice(@PathVariable(value = "Id") Long Id) {
        HashMap message= new HashMap();
        try{
            iservvice.delete(Id);
            message.put("etat","Service deleted");
            return message;
        }
        catch (Exception e) {
            message.put("etat","Service not deleted");
            return message;
        }
    }

}
