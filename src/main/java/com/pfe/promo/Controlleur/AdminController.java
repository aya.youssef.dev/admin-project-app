package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IAdmin;
import com.pfe.promo.dao.IAuthority;
import com.pfe.promo.models.Admin;
import com.pfe.promo.models.Authority;
import com.pfe.promo.models.UserTokenState;
import com.pfe.promo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/admin")

public class AdminController {

        @Autowired
        private IAdmin iadmin;

        @Autowired
        private StorageService storage;

         @Autowired
         private IAuthority iAuthority;

        @GetMapping("/all")
        public List<Admin> getAlladmins() {
            return iadmin.findAll();
        }

        @PostMapping("/save")
        public Admin saveadmin(@RequestBody Admin a) {
            return iadmin.save(a);
        }

        @PutMapping("/update/{Id}")
        public Admin update(@RequestBody Admin a, @PathVariable Long Id) {
            a.setId(Id);
            return iadmin.saveAndFlush(a);
        }

    @RequestMapping("/registeradmin")
    public ResponseEntity<?> register(Admin user, @RequestParam("file") Optional<MultipartFile> file )
    {
        Authority authority=new Authority();
        authority.setName("ADMIN");
        iAuthority.save(authority);
        user.setRole("ADMIN");
        user.setEnabled(true);
        user.setAuthorities(authority);
        user.setPassword(hash(user.getPassword()));
        if (file.isPresent()){
            storage.store(file.get());
            user.setImage(file.get().getOriginalFilename());
        }


        iadmin.save(user);

        return ResponseEntity.ok(new UserTokenState(null, 0, user));

    }
    String hash(String password) {


        String hashedPassword = null;
        int i = 0;
        while (i < 5) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            hashedPassword = passwordEncoder.encode(password);
            i++;
        }

        return hashedPassword;
    }

        @DeleteMapping("/delete/{Id}")
        public HashMap<String, String> deleteLivraison(@PathVariable(value = "Id") Long Id) {
            HashMap message = new HashMap();
            try {
                iadmin.delete(Id);
                message.put("etat", "admin deleted");
                return message;
            } catch (Exception e) {
                message.put("etat", "admin not deleted");
                return message;
            }
        }
    }

