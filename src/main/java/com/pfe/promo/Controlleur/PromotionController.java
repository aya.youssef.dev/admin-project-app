package com.pfe.promo.Controlleur;

import com.pfe.promo.dao.IPromotion;
import com.pfe.promo.models.Promotion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users/promotion")
public class PromotionController {
    @Autowired
    private IPromotion ipromotion;

    @GetMapping("/all")
    public List<Promotion> getAllpromotions() {
        return ipromotion.findAll();
    }

    @PostMapping("/save")
    public Promotion savepromotion(@RequestBody Promotion p) {
        return ipromotion.save(p);
    }

    @PutMapping("/update/{Id}")
    public Promotion update(@RequestBody Promotion p, @PathVariable Long Id) {
        p.setId(Id);
        return ipromotion.saveAndFlush(p);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deletePromotion(@PathVariable(value = "Id") Long Id) {
        HashMap message = new HashMap();
        try {
            ipromotion.delete(Id);
            message.put("etat", "promotion deleted");
            return message;
        } catch (Exception e) {
            message.put("etat", "promotion not deleted");
            return message;
        }
    }
}
