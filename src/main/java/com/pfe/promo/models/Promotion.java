package com.pfe.promo.models;

import javax.persistence.*;

@Entity
public class Promotion {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String prix;




    @ManyToOne
    @JoinColumn(name ="id_article")
    private Article article;

    @ManyToOne
    @JoinColumn(name ="id_servvice")
    private Servvice servvice;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }





    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }


    public Servvice getServvice() {
        return servvice;
    }

    public void setServvice(Servvice servvice) {
        this.servvice = servvice;
    }
}
