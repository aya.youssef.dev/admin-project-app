package com.pfe.promo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Subcategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String namesub;
    private String description;

    @OneToMany(mappedBy ="Subcategory")
    private Collection<Article> articles;

    @ManyToOne
    @JoinColumn(name ="id_categorie")
    private Categorie categorie;

    @OneToMany(mappedBy ="subcategory")
    private Collection<Servvice> servvice;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamesub() {
        return namesub;
    }

    public void setNamesub(String namesub) {
        this.namesub = namesub;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Collection<Article> getArticles() {
        return articles;
    }

    public void setArticles(Collection<Article> articles) {
        this.articles = articles;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

@JsonIgnore
    public Collection<Servvice> getServvice() {
        return servvice;
    }

    public void setServvice(Collection<Servvice> servvice) {
        this.servvice = servvice;
    }
}
