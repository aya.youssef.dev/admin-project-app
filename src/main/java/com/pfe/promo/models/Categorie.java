package com.pfe.promo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
@Entity
public class Categorie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String genre;
    private String description;



    @OneToMany (mappedBy = "categorie")
    private Collection<Subcategory> subcategorys;





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
@JsonIgnore
    public Collection<Subcategory> getSubcategorys() {
        return subcategorys;
    }

    public void setSubcategorys(Collection<Subcategory> subcategorys) {
        this.subcategorys = subcategorys;
    }
}
