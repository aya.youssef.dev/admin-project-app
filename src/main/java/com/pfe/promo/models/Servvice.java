package com.pfe.promo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity

public class Servvice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private String image;
    private String tel;
    private String nameservice;
    private String descriptionservice;
    private String prixservice;
    private  String adresse;

    @ManyToOne
    @JoinColumn(name ="id_subcategory")
    private Subcategory subcategory;

    @ManyToOne
    @JoinColumn(name ="id_entreprise")
    private Entreprise entreprise;

    @OneToMany(mappedBy = "servvice")
    private Collection<Promotion> promotion;

    public String getTel() {
        return tel;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getNameservice() {
        return nameservice;
    }

    public void setNameservice(String nameservice) {
        this.nameservice = nameservice;
    }

    public String getDescriptionservice() {
        return descriptionservice;
    }

    public void setDescriptionservice(String descriptionservice) {
        this.descriptionservice = descriptionservice;
    }

    public String getPrixservice() {
        return prixservice;
    }

    public void setPrixservice(String prixservice) {
        this.prixservice = prixservice;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
@JsonIgnore
    public Collection<Promotion> getPromotion() {
        return promotion;
    }

    public void setPromotion(Collection<Promotion> promotion) {
        this.promotion = promotion;
    }

}
