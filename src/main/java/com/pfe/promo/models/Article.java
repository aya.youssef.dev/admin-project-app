package com.pfe.promo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String prix;
    private String Description;
    private String Remise ;
    private String Quantite;
    private String image;

    @ManyToMany (mappedBy = "article")
    private Collection<Panier> paniers;

    @ManyToOne
    @JoinColumn(name ="id_client")
    private Client client;

    @OneToMany(mappedBy ="article")
    private Collection<Promotion> promotions;

    @ManyToOne
    @JoinColumn(name ="id_subcategory")
    private Subcategory Subcategory;

    @ManyToOne
    @JoinColumn(name ="id_entreprise")
    private Entreprise entreprise;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getRemise() {
        return Remise;
    }

    public void setRemise(String remise) {
        Remise = remise;
    }

    public String getQuantite() {
        return Quantite;
    }

    public void setQuantite(String quantite) {
        Quantite = quantite;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }





    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    @JsonIgnore

    public Collection<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(Collection<Promotion> promotions) {
        this.promotions = promotions;
    }

    public Subcategory getSubcategory() {
        return Subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        Subcategory = subcategory;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Collection<Panier> getPaniers() {
        return paniers;
    }

    @JsonIgnore
    public void setPaniers(Collection<Panier> paniers) {
        this.paniers = paniers;
    }
}
