package com.pfe.promo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Panier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String total;



    @ManyToMany
    @JoinTable(name="hist_panier",
    joinColumns=@JoinColumn(name = "id_panier"),inverseJoinColumns=@JoinColumn(name = "id_article"))
    private Collection<Article>article;


    @OneToMany(mappedBy ="panier")
    private Collection <Commande> commandes;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
    @JsonIgnore

    public Collection<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(Collection<Commande> commandes) {
        this.commandes = commandes;
    }

    public Collection<Article> getArticles() {
        return article;
    }

    public void setArticles(Collection<Article> articles) {
        this.article = articles;
    }
}
