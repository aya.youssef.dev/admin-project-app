package com.pfe.promo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Entreprise extends User{

    private String adresse;
    private String tel;
    private String email;


    @OneToMany(mappedBy = "entreprise")
    private Collection<Article> articles;

    @OneToMany(mappedBy = "entreprise")
    private Collection<Servvice> servvices;



    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
@JsonIgnore
    public Collection<Article> getArticles() {
        return articles;
    }

    public void setArticles(Collection<Article> articles) {
        this.articles = articles;
    }

    @JsonIgnore
    public Collection<Servvice> getServvices() {
        return servvices;
    }

    public void setServvices(Collection<Servvice> servvices) {
        this.servvices = servvices;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
